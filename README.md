> #### Fruitzplon

> Prérequis :
> #### Prérequis :
>  - [Java Version 17 installée]
>  - [Un navigateur web]
>  - [Une base de données SQL]


> 1. Cloner le repository  https://gitlab.com/MaidenQuinn/fruitzplon-api
>  2. Ouvrir le projet dans un IDE
>  3. Créer une BDD nommée "fruitzplon"
>  4. Dans cette BDD, importer premièrement le doc "database.sql" situé dans le dossier "/fruitzplon/src/main/resources/   database.sql"
>  5. Faire de même pour le document "data.sql" situé au même endroit
>  6. Lancer le projet Java via votre IDE
>  7. Se rendre sur l'URL  http://localhost:8080/
>  8. Un compte par défaut est disponible : totod@gmail.com, mdp: test1234
