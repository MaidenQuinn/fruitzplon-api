package co.simplon.alt3.fruitzplon.entity;

import java.util.ArrayList;
import java.util.List;

public class Fruit {
    private Integer id;
    private String name;
    private Category category;
    private Season season;
    private String description;
    private String nutritionFacts;
    private String imageUrl;
    private List<Recipe> recipes;

    public Fruit(Integer id, String name, String description, String nutritionFacts, String imageUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.nutritionFacts = nutritionFacts;
        this.imageUrl = imageUrl;
        this.recipes = new ArrayList<>();
    }

    public Fruit(String name, Category category, Season season, String description, String nutritionFacts,
            String imageUrl) {
        this.name = name;
        this.category = category;
        this.season = season;
        this.description = description;
        this.nutritionFacts = nutritionFacts;
        this.imageUrl = imageUrl;
        this.recipes = new ArrayList<>();
    }

    public Fruit(Integer id, String name, Category category, Season season, String description, String nutritionFacts,
            String imageUrl) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.season = season;
        this.description = description;
        this.nutritionFacts = nutritionFacts;
        this.imageUrl = imageUrl;
        this.recipes = new ArrayList<>();
    }

    public Fruit() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNutritionFacts() {
        return nutritionFacts;
    }

    public void setNutritionFacts(String nutritionFacts) {
        this.nutritionFacts = nutritionFacts;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

}
