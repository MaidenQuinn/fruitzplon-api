package co.simplon.alt3.fruitzplon.entity;

import java.sql.Timestamp;

public class Season {
    private Integer id;
    private String name;
    private Timestamp startDate;
    private Timestamp endDate;

    public Season() {
    }

    public Season(String name, Timestamp startDate, Timestamp endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Season(Integer id, String name, Timestamp startDate, Timestamp endDate) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

}
