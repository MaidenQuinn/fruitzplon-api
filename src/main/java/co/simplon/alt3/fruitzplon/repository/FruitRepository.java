package co.simplon.alt3.fruitzplon.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.alt3.fruitzplon.entity.Category;
import co.simplon.alt3.fruitzplon.entity.Fruit;
import co.simplon.alt3.fruitzplon.entity.Recipe;
import co.simplon.alt3.fruitzplon.entity.Season;

@Repository
public class FruitRepository implements IRepository<Fruit> {

    private Connection connection;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SeasonRepository seasonRepository;
    @Autowired
    private RecipeRepository recipeRepository;

    public FruitRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/fruitzplon");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Fruit> findAll() {
        List<Fruit> fruitList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM fruit");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Fruit fruit = new Fruit(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("description"),
                        result.getString("nutritionFacts"),
                        result.getString("imageUrl"));
                Category category = categoryRepository.findById(result.getInt("category_id"));
                fruit.setCategory(category);
                Season season = seasonRepository.findById(result.getInt("season_id"));
                fruit.setSeason(season);
                List<Recipe> recipeList = recipeRepository.findByFruit(result.getInt("id"));
                fruit.setRecipes(recipeList);
                fruitList.add(fruit);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fruitList;
    }

    @Override
    public Fruit findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM fruit WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            result.next();

            if (result.getInt("id") == id) {
                Fruit fruit = new Fruit(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("description"),
                        result.getString("nutritionFacts"),
                        result.getString("imageUrl"));
                Category category = categoryRepository.findById(result.getInt("category_id"));
                fruit.setCategory(category);
                Season season = seasonRepository.findById(result.getInt("season_id"));
                fruit.setSeason(season);
                List<Recipe> recipeList = recipeRepository.findByFruit(result.getInt("id"));
                fruit.setRecipes(recipeList);
                return fruit;
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean save(Fruit fruit) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO fruit(name, category_id, season_id, description, nutritionFacts, imageUrl) VALUES(?, ?, ?, ?, ?, ?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, fruit.getName());
            stmt.setInt(2, fruit.getCategory().getId());
            stmt.setInt(3, fruit.getSeason().getId());
            stmt.setString(4, fruit.getDescription());
            stmt.setString(5, fruit.getNutritionFacts());
            stmt.setString(6, fruit.getImageUrl());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                fruit.setId(result.getInt(1));
                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;

    }

    @Override
    public void update(Fruit fruit) {
        try {
            PreparedStatement stmt = connection
                    .prepareStatement(
                            "UPDATE fruit SET name=?, category_id=?, season_id=?, description=?, nutritionFacts=?, imageUrl=? WHERE id=?");
            stmt.setString(1, fruit.getName());
            stmt.setInt(2, fruit.getCategory().getId());
            stmt.setInt(3, fruit.getSeason().getId());
            stmt.setString(4, fruit.getDescription());
            stmt.setString(5, fruit.getNutritionFacts());
            stmt.setString(6, fruit.getImageUrl());
            stmt.setInt(7, fruit.getId());
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    @Override
    public void deleteById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM fruit WHERE id=?");
            stmt.setInt(1, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
