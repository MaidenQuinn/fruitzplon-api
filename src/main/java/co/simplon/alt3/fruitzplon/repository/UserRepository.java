package co.simplon.alt3.fruitzplon.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import co.simplon.alt3.fruitzplon.entity.User;

@Repository
public class UserRepository implements IRepository<User>, UserDetailsService {

    @Autowired
    private DataSource dataSource;
    private Connection connection;

    public UserRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/fruitzplon");
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public List<User> findAll() {
        List<User> userList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                User user = new User(
                        result.getInt("userId"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getString("email"),
                        result.getString("userPassword"));

                userList.add(user);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return userList;
    }

    public User findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE userId=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("userId"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getString("email"),
                        result.getString("userPassword"));

            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    public boolean save(User entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO user(firstName, lastName, email, password) VALUES(?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, entity.getFirstName());
            stmt.setString(2, entity.getLastName());
            stmt.setString(3, entity.getEmail());
            stmt.setString(4, entity.getPassword());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                entity.setId(result.getInt(1));

                return true;

            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;
    }

    public void update(User entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE user SET firstName=?, lastName=?, email=?, password=? WHERE id=?");
            stmt.setString(1, entity.getFirstName());
            stmt.setString(2, entity.getLastName());
            stmt.setString(3, entity.getEmail());
            stmt.setString(4, entity.getPassword());
            stmt.setInt(5, entity.getId());

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    public void deleteById(int userId) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM user WHERE userId=?");
            stmt.setInt(1, userId);
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    public User findByEmail(String email) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("id"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getString("email"),
                        result.getString("password"));
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
}
