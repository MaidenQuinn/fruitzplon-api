package co.simplon.alt3.fruitzplon.repository;

import java.util.List;


public interface IRepository <T> {

    List<T> findAll();
    
    public T findById(int id);

    public boolean save(T entity);

    public void update(T entity);

    public void deleteById(int id);
    
}
