package co.simplon.alt3.fruitzplon.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

import co.simplon.alt3.fruitzplon.entity.Category;

@Repository
public class CategoryRepository implements IRepository<Category> {

    private Connection connection;

    public CategoryRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/fruitzplon");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Category> findAll() {
        List<Category> categoryList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Category category = new Category(
                        result.getInt("id"),
                        result.getString("name"));

                categoryList.add(category);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return categoryList;
    }

    @Override
    public Category findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            result.next();

            if (result.getInt("id") == id) {
                return new Category(
                        result.getInt("id"),
                        result.getString("name"));
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean save(Category category) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO category(name) VALUES(?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, category.getName());
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                category.getId();

                return true;

            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void update(Category category) {
        try {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE category SET name=? WHERE id=?");
            stmt.setString(1, category.getName());
            stmt.setInt(2, category.getId());

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    @Override
    public void deleteById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM category WHERE id=?");
            stmt.setInt(1, id);
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

}
