package co.simplon.alt3.fruitzplon.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.alt3.fruitzplon.entity.Season;

@Repository
public class SeasonRepository implements IRepository<Season> {

    private Connection connection;

    public SeasonRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/fruitzplon");
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    @Override
    public List<Season> findAll() {
        List<Season> seasonList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM season");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Season season = new Season(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getTimestamp("startDate"),
                        result.getTimestamp("endDate"));

                seasonList.add(season);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return seasonList;
    }

    @Override
    public Season findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM season WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            result.next();

            if (result.getInt("id") == id) {
                return new Season(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getTimestamp("startDate"),
                        result.getTimestamp("endDate"));
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean save(Season entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO season(name, startDate, endDate) VALUES(?, ?, ?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, entity.getName());
            stmt.setTimestamp(2, entity.getStartDate());
            stmt.setTimestamp(3, entity.getEndDate());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                entity.setId(result.getInt(1));

                return true;

            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void update(Season entity) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE season SET name=?, startDate=?, endDate=? WHERE id=?");
            stmt.setString(1, entity.getName());
            stmt.setTimestamp(2, entity.getStartDate());
            stmt.setTimestamp(3, entity.getEndDate());
            stmt.setInt(4, entity.getId());

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    @Override
    public void deleteById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM season WHERE id=?");
            stmt.setInt(1, id);
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

}
