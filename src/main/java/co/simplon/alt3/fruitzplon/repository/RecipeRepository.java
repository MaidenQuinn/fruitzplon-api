package co.simplon.alt3.fruitzplon.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import co.simplon.alt3.fruitzplon.entity.Recipe;

@Repository
public class RecipeRepository implements IRepository<Recipe> {

    private Connection connection;

    public RecipeRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/fruitzplon");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Recipe> findAll() {
        List<Recipe> recipeList = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM recipe");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Recipe recipe = new Recipe(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("description"),
                        result.getString("imageUrl"));

                recipeList.add(recipe);
            }

            return recipeList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Recipe findById(int id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM recipe WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            result.next();

            if (result.getInt("id") == id) {
                Recipe recipe = new Recipe(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("description"),
                        result.getString("imageUrl"));

                return recipe;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Recipe> findByFruit(int id) {
        List<Recipe> recipeList = new ArrayList<>();

        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM recipe WHERE fruit_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Recipe recipe = new Recipe(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("description"),
                        result.getString("imageUrl"));

                recipeList.add(recipe);
            }
            return recipeList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean save(Recipe recipe) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO recipe(name, description, imageUrl, fruit_id) VALUES(?, ?, ?, ?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, recipe.getName());
            stmt.setString(2, recipe.getDescription());
            stmt.setString(3, recipe.getImageUrl());
            stmt.setInt(4, recipe.getFruit().getId());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                recipe.getId();

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void update(Recipe recipe) {
        try {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE recipe SET name=?, description=?, imageUrl=?, fruit_id=? WHERE id=?");
            stmt.setString(1, recipe.getName());
            stmt.setString(2, recipe.getDescription());
            stmt.setString(3, recipe.getImageUrl());
            stmt.setInt(4, recipe.getFruit().getId());
            stmt.setInt(5, recipe.getId());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void deleteById(int id) {
        try {
            PreparedStatement stmt = connection
                    .prepareStatement("DELETE FROM recipe WHERE id=?");
            stmt.setInt(1, id);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
