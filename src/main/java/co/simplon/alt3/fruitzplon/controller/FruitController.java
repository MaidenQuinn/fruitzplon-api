package co.simplon.alt3.fruitzplon.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.util.concurrent.RateLimiter;

import co.simplon.alt3.fruitzplon.entity.Fruit;
import co.simplon.alt3.fruitzplon.entity.User;
import co.simplon.alt3.fruitzplon.repository.CategoryRepository;
import co.simplon.alt3.fruitzplon.repository.FruitRepository;
import co.simplon.alt3.fruitzplon.repository.SeasonRepository;
import co.simplon.alt3.fruitzplon.service.Uploader;

@Controller
public class FruitController {

    @Autowired
    private FruitRepository fruitRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private Uploader uploader;


    RateLimiter rateLimiter = RateLimiter.create(1);

    @GetMapping("/fruit/{id}")
    public String showFruit(@PathVariable int id, Authentication authentication, Model model) {
        rateLimiter.acquire(1);
        if (authentication != null) {
            User user = (User) authentication.getPrincipal();
            model.addAttribute("user", user);
        }
        model.addAttribute("fruit", fruitRepository.findById(id));
        return "fruit";
    }

    @GetMapping("/fruits")
    public List<Fruit> listFruits() {
        rateLimiter.acquire(1);
        List<Fruit> fruits = new ArrayList<>(fruitRepository.findAll());
        return fruits;
    }

    @GetMapping("/add-fruit")
    public String showAddFruitForm(Authentication authentication, Model model) {
        rateLimiter.acquire(1);
        if (authentication != null) {
            User user = (User) authentication.getPrincipal();
            model.addAttribute("user", user);
        }
        model.addAttribute("fruit", new Fruit());
        model.addAttribute("categoryList", categoryRepository.findAll());
        model.addAttribute("seasonList", seasonRepository.findAll());
        return "add-fruit";
    }

    @PostMapping("/add-fruit")
    public String addFruit(Fruit fruit, @RequestParam("file") MultipartFile file) {
        rateLimiter.acquire(1);
        try {
            fruit.setImageUrl(uploader.upload(file));
            fruitRepository.save(fruit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }

}
