package co.simplon.alt3.fruitzplon.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.google.common.util.concurrent.RateLimiter;

import co.simplon.alt3.fruitzplon.entity.User;

@Controller
public class AccountController {

    RateLimiter rateLimiter = RateLimiter.create(1);

    @GetMapping("/account")
    public String showAccount(Authentication authentication, Model model) {
        rateLimiter.acquire(1);
        if (authentication != null) {
            User user = (User) authentication.getPrincipal();
            model.addAttribute("user", user);
        }
        return "account";
    }

}
