package co.simplon.alt3.fruitzplon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.google.common.util.concurrent.RateLimiter;

import co.simplon.alt3.fruitzplon.repository.CategoryRepository;

@Controller
public class CategoryController {
    
    @Autowired
    CategoryRepository categoryRepo;
    RateLimiter rateLimiter = RateLimiter.create(1);

    @GetMapping("/category")
    public String showCategory(Model model) {
        rateLimiter.acquire(1);
        model.addAttribute("categoryList", categoryRepo.findAll());
        return "category";
    }

}
