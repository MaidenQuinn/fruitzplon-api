package co.simplon.alt3.fruitzplon.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.google.common.util.concurrent.RateLimiter;

import co.simplon.alt3.fruitzplon.entity.Fruit;
import co.simplon.alt3.fruitzplon.entity.User;
import co.simplon.alt3.fruitzplon.repository.FruitRepository;

@Controller
public class HomeController {
    @Autowired
    private FruitRepository fruitRepository;

    RateLimiter rateLimiter = RateLimiter.create(1);

    @GetMapping("/")
    public String showIndex(Authentication authentication, Model model) {
        rateLimiter.acquire(1);
        List<Fruit> fruits = fruitRepository.findAll();
        if (authentication != null) {
            User user = (User) authentication.getPrincipal();
            model.addAttribute("user", user);
        }
        if (fruits.size() > 9) {
            Collections.reverse(fruits);
            model.addAttribute("fruits", fruits.subList(0, 10));
        } else {
            model.addAttribute("fruits", fruits);
        }
        return "index";
    }

}
