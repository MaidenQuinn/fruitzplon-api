package co.simplon.alt3.fruitzplon.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.google.common.util.concurrent.RateLimiter;

import co.simplon.alt3.fruitzplon.entity.User;
import co.simplon.alt3.fruitzplon.repository.UserRepository;

@Controller
public class AuthController {

    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UserRepository repo;

    RateLimiter rateLimiter = RateLimiter.create(1);


    @GetMapping("/register")
    public String showRegister(Model model) {
        rateLimiter.acquire(1);
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid User user, BindingResult result, Model model) {
        rateLimiter.acquire(1);
        if (result.hasErrors()) {
            return "register";

        }
        if (repo.findByEmail(user.getEmail()) != null) {
            model.addAttribute("feedback", "L'utilisateur existe déjà");

            return "register";
        }
        String hashedPassword = encoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        repo.save(user);
        return "redirect:/";
    }
}
