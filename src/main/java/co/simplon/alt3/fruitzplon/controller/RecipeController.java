package co.simplon.alt3.fruitzplon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.util.concurrent.RateLimiter;

import co.simplon.alt3.fruitzplon.entity.Recipe;
import co.simplon.alt3.fruitzplon.entity.User;
import co.simplon.alt3.fruitzplon.repository.FruitRepository;
import co.simplon.alt3.fruitzplon.repository.RecipeRepository;
import co.simplon.alt3.fruitzplon.service.Uploader;

@Controller
public class RecipeController {
    
    @Autowired
    private RecipeRepository recipeRepo;
    @Autowired
    private FruitRepository fruitRepo;
    @Autowired
    private Uploader uploader;

    RateLimiter rateLimiter = RateLimiter.create(1);


    @GetMapping("/recipe/{id}")
    public String showRecipe(Authentication authentication, @PathVariable int id, Model model) {
        rateLimiter.acquire(1);
        if (authentication != null) {
            User user = (User) authentication.getPrincipal();
            model.addAttribute("user", user);
        }
        model.addAttribute("recipe", recipeRepo.findById(id));
        return "recipe";
    }

    @GetMapping("/add-recipe")
    public String addRecipeForm(Authentication authentication, Model model) {
        rateLimiter.acquire(1);
        if (authentication != null) {
            User user = (User) authentication.getPrincipal();
            model.addAttribute("user", user);
        }
        model.addAttribute("recipe", new Recipe());
        model.addAttribute("fruitList", fruitRepo.findAll());
        return "add-recipe";
    }

    @PostMapping("/add-recipe")
    public String addRecipe(Authentication authentication, Recipe recipe, @RequestParam("file") MultipartFile file) {
        rateLimiter.acquire(1);
        try {
            User user = (User) authentication.getPrincipal();
            recipe.setUser(user); 
            recipe.setImageUrl(uploader.upload(file));
            recipeRepo.save(recipe);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }
}
