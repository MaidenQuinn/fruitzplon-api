package co.simplon.alt3.fruitzplon.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class Uploader {

    public boolean validateFileExtn(MultipartFile file) {
        String type = file.getContentType();
        if (type.equals("image/png") || type.equals("image/jpeg") || type.equals("image/jpg")
                || type.equals("image/gif")) {
            return true;
        }
        return false;
    }

    public String upload(MultipartFile file) throws IOException {

        if (validateFileExtn(file)) {

            String fileName = "/images/" + UUID.randomUUID() + "."
                    + StringUtils.getFilenameExtension(file.getOriginalFilename());

            Path path = Paths.get("src/main/resources/static" + fileName);
            Files.copy(file.getInputStream(), path);

            return fileName;

        }
        throw new Error("Le format du document n'est pas valide");
    }
}
