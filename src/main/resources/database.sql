CREATE DATABASE IF NOT EXISTS fruitzplon;

USE fruitzplon;

DROP TABLE IF EXISTS user;

DROP TABLE IF EXISTS recipe;

DROP TABLE IF EXISTS fruit;

DROP TABLE IF EXISTS category;

DROP TABLE IF EXISTS season;


CREATE TABLE
    IF NOT EXISTS user(
        id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        firstName VARCHAR(255),
        lastName VARCHAR(255),
        email VARCHAR(255) UNIQUE KEY NOT NULL,
        password VARCHAR(255) NOT NULL
    );

CREATE TABLE
    IF NOT EXISTS category (
        id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL
    );

CREATE TABLE
    IF NOT EXISTS season (
        id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        startDate TIMESTAMP NOT NULL,
        endDate TIMESTAMP NOT NULL
    );

CREATE TABLE
    IF NOT EXISTS fruit (
        id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        description TEXT NOT NULL,
        nutritionFacts VARCHAR(255) NOT NULL,
        imageUrl VARCHAR(255) NOT NULL,
        category_id INTEGER NOT NULL,
        season_id INTEGER NOT NULL,
        FOREIGN KEY (category_id) REFERENCES category(id),
        FOREIGN KEY (season_id) REFERENCES season(id)
    );

CREATE TABLE
    IF NOT EXISTS recipe (
        id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        description TEXT NOT NULL,
        fruit_id INTEGER NOT NULL,
        imageUrl VARCHAR(255) NOT NULL,
        FOREIGN KEY (fruit_id) REFERENCES fruit(id)
    );