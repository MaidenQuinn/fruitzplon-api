INSERT INTO
    user (
        firstName,
        lastName,
        email,
        password
    )
VALUES (
        'Toto',
        'Dupont',
        'totod@gmail.com',
        '$2a$10$m8OZiNCDi.O32gfMTzPEvumAiJ6Bas3jqdr16CwIsjpXjcS7KuXNW'
    );

-- mdp en clair : test1234

INSERT INTO category (name)
VALUES ('noyau'), ('pépin'), ('baies, fruits rouge'), ('agrumes'), ('coque'), ('exotique');

INSERT INTO
    season(name, startDate, endDate)
VALUES (
        'Printemps',
        "2022-03-20",
        "2022-06-20"
    ), (
        'Été',
        "2022-06-21",
        "2022-09-22"
    ), (
        'Automne',
        "2022-09-23",
        "2022-12-20"
    ), (
        'Hiver',
        "2022-12-21",
        "2023-03-19"
    );

INSERT INTO
    fruit(
        name,
        description,
        nutritionFacts,
        imageUrl,
        category_id,
        season_id
    )
VALUES (
        "Banane",
        "La banane est le fruit ou la baie dérivant de l’inflorescence du bananier. 
            Les bananes sont des fruits très généralement stériles issus de variétés domestiquées. 
            Seuls les fruits des bananiers sauvages et de quelques cultivars domestiques contiennent des graines. 
            Les bananes sont généralement jaunes avec des taches brunâtres lorsqu'elles sont mûres et vertes quand elles ne le sont pas.

        Les bananes constituent un élément essentiel du régime alimentaire dans certaines régions, comme en Ouganda, 
        qui offrirait une cinquantaine de variétés de ce fruit.",
        "90,50 kcal pour 100 g soit 383 kJ",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Bananas_white_background_DS.jpg/1280px-Bananas_white_background_DS.jpg",
        3,
        2
    ), (
        "Clémentine",
        "La clémentine est un agrume, fruit du clémentinier (Citrus × clementina), un arbre hybride de la famille des rutacées1, 
            issu du croisement entre un mandarinier (Citrus reticulata) et un oranger (Citrus sinensis).",
        "47,30 kcal pour 100 g soit 200 kJ",
        "https://upload.wikimedia.org/wikipedia/commons/a/a9/Oh_my_darling.jpg",
        4,
        3
    );

INSERT INTO
    recipe (
        id,
        name,
        description,
        fruit_id,
        imageUrl
    )
VALUES (
        1,
        "Muffin à la banane",
        "ÉTAPE 1
            Ecraser les bananes. Ajouter le sucre et l'oeuf légèrement battu. Ajouter le beurre fondu, puis les ingrédients secs (farine, levure, bicarbonate).

            ÉTAPE 2
            Placer dans des moules à muffin.

            ÉTAPE 3
            Cuire à 190°C pendant 20 min.",
        1,
        "https://assets.afcdn.com/recipe/20150225/373_w1000h1396c1cx1250cy1745.webp"
    ), (
        2,
        "Beignet à la banane",
        "ÉTAPE 1

            Ecraser les bananes puis ajouter les oeufs.

            ÉTAPE 2
            sucre
            Rajouter le sucre, mélanger.

            ÉTAPE 3
            farine
            Rajouter la farine, mélanger.

            ÉTAPE 4
            Faire cuire dans l'huile.",
        1,
        "https://assets.afcdn.com/recipe/20160719/15567_w1000h1500c1cx1424cy2136.webp"
    ), (
        3,
        "Gâteau yaourt aux clémentines",
        "ÉTAPE 1
            sucre en poudre
            Versez le sucre en poudre et les œufs dans un saladier, puis blanchissez l'ensemble.

            ÉTAPE 2
            yaourt nature
            beurre
            Ajoutez le yaourt nature ainsi que le beurre fondu.

            ÉTAPE 3
            farine
            levure chimique
            sel
            Mélangez le tout. Ajoutez la farine tamisée,la levure chimique et la pincée de sel à la préparation.

            ÉTAPE 4
            clémentine
            Épluchez les clémentines, séparez les en quartiers et ajoutez les à la préparation.

            ÉTAPE 5
            Mélangez bien l'ensemble.

            ÉTAPE 6
            Versez le tout dans un moule à manquer (beurré et fariné) et faites cuire dans un four préchauffé à 180°C (Thermostat 6) pendant 40 minutes environ.

            ÉTAPE 7
            Vérifiez la cuisson à l'aide d'un couteau : si la lame ressort sans trace de pâte, le gâteau est cuit.",
        2,
        "https://assets.afcdn.com/recipe/20160414/32414_w640h486c1cx2880cy1920.webp"
    );